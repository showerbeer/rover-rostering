﻿namespace WindowsFormsApplication1
{
    partial class Rover_staff_manager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.add_rover = new System.Windows.Forms.Button();
            this.list_of_rovers = new System.Windows.Forms.ListBox();
            this.remove_rover = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // add_rover
            // 
            this.add_rover.Location = new System.Drawing.Point(12, 12);
            this.add_rover.Name = "add_rover";
            this.add_rover.Size = new System.Drawing.Size(97, 23);
            this.add_rover.TabIndex = 0;
            this.add_rover.Text = "Add new Rover";
            this.add_rover.UseVisualStyleBackColor = true;
            this.add_rover.Click += new System.EventHandler(this.add_rover_Click);
            // 
            // list_of_rovers
            // 
            this.list_of_rovers.FormattingEnabled = true;
            this.list_of_rovers.Location = new System.Drawing.Point(238, 12);
            this.list_of_rovers.Name = "list_of_rovers";
            this.list_of_rovers.Size = new System.Drawing.Size(223, 355);
            this.list_of_rovers.TabIndex = 1;
            this.list_of_rovers.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // remove_rover
            // 
            this.remove_rover.Location = new System.Drawing.Point(12, 54);
            this.remove_rover.Name = "remove_rover";
            this.remove_rover.Size = new System.Drawing.Size(97, 23);
            this.remove_rover.TabIndex = 2;
            this.remove_rover.Text = "Remove Rover";
            this.remove_rover.UseVisualStyleBackColor = true;
            this.remove_rover.Click += new System.EventHandler(this.remove_rover_Click);
            // 
            // Rover_staff_manager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(473, 384);
            this.Controls.Add(this.remove_rover);
            this.Controls.Add(this.list_of_rovers);
            this.Controls.Add(this.add_rover);
            this.Name = "Rover_staff_manager";
            this.Text = "Your staffing solution";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button add_rover;
        private System.Windows.Forms.ListBox list_of_rovers;
        private System.Windows.Forms.Button remove_rover;
    }
}

