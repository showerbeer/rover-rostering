﻿namespace WindowsFormsApplication1
{
    partial class add_new_rover
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rover_name = new System.Windows.Forms.TextBox();
            this.rover_phone_number = new System.Windows.Forms.TextBox();
            this.rover_alternate_phone_number = new System.Windows.Forms.TextBox();
            this.submit_new_rover_form = new System.Windows.Forms.Button();
            this.clear_add_new_rover_forms = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // rover_name
            // 
            this.rover_name.Location = new System.Drawing.Point(37, 32);
            this.rover_name.Name = "rover_name";
            this.rover_name.Size = new System.Drawing.Size(205, 20);
            this.rover_name.TabIndex = 0;
            this.rover_name.Text = "Name";
            this.rover_name.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            this.rover_name.Click += new System.EventHandler(this.rover_name_Click);
            // 
            // rover_phone_number
            // 
            this.rover_phone_number.Location = new System.Drawing.Point(37, 75);
            this.rover_phone_number.Name = "rover_phone_number";
            this.rover_phone_number.Size = new System.Drawing.Size(205, 20);
            this.rover_phone_number.TabIndex = 1;
            this.rover_phone_number.Text = "Phone number";
            this.rover_phone_number.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            this.rover_phone_number.Click += new System.EventHandler(this.rover_phone_number_Click);
            // 
            // rover_alternate_phone_number
            // 
            this.rover_alternate_phone_number.Location = new System.Drawing.Point(37, 120);
            this.rover_alternate_phone_number.Name = "rover_alternate_phone_number";
            this.rover_alternate_phone_number.Size = new System.Drawing.Size(205, 20);
            this.rover_alternate_phone_number.TabIndex = 2;
            this.rover_alternate_phone_number.Text = "Alternate phone number";
            this.rover_alternate_phone_number.TextChanged += new System.EventHandler(this.rover_alternate_phone_number_TextChanged);
            this.rover_alternate_phone_number.Click += new System.EventHandler(this.rover_alternate_phone_number_Click);
            // 
            // submit_new_rover_form
            // 
            this.submit_new_rover_form.Location = new System.Drawing.Point(37, 168);
            this.submit_new_rover_form.Name = "submit_new_rover_form";
            this.submit_new_rover_form.Size = new System.Drawing.Size(75, 23);
            this.submit_new_rover_form.TabIndex = 3;
            this.submit_new_rover_form.Text = "Submit";
            this.submit_new_rover_form.UseVisualStyleBackColor = true;
            this.submit_new_rover_form.Click += new System.EventHandler(this.submit_new_rover_form_Click);
            // 
            // clear_add_new_rover_forms
            // 
            this.clear_add_new_rover_forms.Location = new System.Drawing.Point(167, 168);
            this.clear_add_new_rover_forms.Name = "clear_add_new_rover_forms";
            this.clear_add_new_rover_forms.Size = new System.Drawing.Size(75, 23);
            this.clear_add_new_rover_forms.TabIndex = 4;
            this.clear_add_new_rover_forms.Text = "Clear";
            this.clear_add_new_rover_forms.UseVisualStyleBackColor = true;
            this.clear_add_new_rover_forms.Click += new System.EventHandler(this.button1_Click);
            // 
            // add_new_rover
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.clear_add_new_rover_forms);
            this.Controls.Add(this.submit_new_rover_form);
            this.Controls.Add(this.rover_alternate_phone_number);
            this.Controls.Add(this.rover_phone_number);
            this.Controls.Add(this.rover_name);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "add_new_rover";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Enter Rover details";
            this.Load += new System.EventHandler(this.add_new_rover_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox rover_name;
        private System.Windows.Forms.TextBox rover_phone_number;
        private System.Windows.Forms.TextBox rover_alternate_phone_number;
        private System.Windows.Forms.Button submit_new_rover_form;
        private System.Windows.Forms.Button clear_add_new_rover_forms;
    }
}