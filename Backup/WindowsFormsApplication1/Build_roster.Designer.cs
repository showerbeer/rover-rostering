﻿namespace WindowsFormsApplication1
{
    partial class Build_roster
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.aic_roster_builder = new System.Windows.Forms.TabPage();
            this.chic_roster_builder = new System.Windows.Forms.TabPage();
            this.huxley_roster_builder = new System.Windows.Forms.TabPage();
            this.list_of_rovers = new System.Windows.Forms.ListBox();
            this.export_rosters = new System.Windows.Forms.Button();
            this.import_roster = new System.Windows.Forms.Button();
            this.manage_rovers = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.aic_roster_builder);
            this.tabControl1.Controls.Add(this.chic_roster_builder);
            this.tabControl1.Controls.Add(this.huxley_roster_builder);
            this.tabControl1.Location = new System.Drawing.Point(2, 1);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(872, 611);
            this.tabControl1.TabIndex = 0;
            // 
            // aic_roster_builder
            // 
            this.aic_roster_builder.Location = new System.Drawing.Point(4, 22);
            this.aic_roster_builder.Name = "aic_roster_builder";
            this.aic_roster_builder.Padding = new System.Windows.Forms.Padding(3);
            this.aic_roster_builder.Size = new System.Drawing.Size(864, 585);
            this.aic_roster_builder.TabIndex = 0;
            this.aic_roster_builder.Text = "AIC Roster";
            this.aic_roster_builder.UseVisualStyleBackColor = true;
            this.aic_roster_builder.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // chic_roster_builder
            // 
            this.chic_roster_builder.Location = new System.Drawing.Point(4, 22);
            this.chic_roster_builder.Name = "chic_roster_builder";
            this.chic_roster_builder.Padding = new System.Windows.Forms.Padding(3);
            this.chic_roster_builder.Size = new System.Drawing.Size(864, 584);
            this.chic_roster_builder.TabIndex = 1;
            this.chic_roster_builder.Text = "CHIC Roster";
            this.chic_roster_builder.UseVisualStyleBackColor = true;
            this.chic_roster_builder.Click += new System.EventHandler(this.tabPage2_Click);
            // 
            // huxley_roster_builder
            // 
            this.huxley_roster_builder.Location = new System.Drawing.Point(4, 22);
            this.huxley_roster_builder.Name = "huxley_roster_builder";
            this.huxley_roster_builder.Padding = new System.Windows.Forms.Padding(3);
            this.huxley_roster_builder.Size = new System.Drawing.Size(864, 584);
            this.huxley_roster_builder.TabIndex = 2;
            this.huxley_roster_builder.Text = "Huxley Roster";
            this.huxley_roster_builder.UseVisualStyleBackColor = true;
            // 
            // list_of_rovers
            // 
            this.list_of_rovers.FormattingEnabled = true;
            this.list_of_rovers.Location = new System.Drawing.Point(880, 23);
            this.list_of_rovers.Name = "list_of_rovers";
            this.list_of_rovers.Size = new System.Drawing.Size(181, 589);
            this.list_of_rovers.TabIndex = 2;
            // 
            // export_rosters
            // 
            this.export_rosters.Location = new System.Drawing.Point(186, 634);
            this.export_rosters.Name = "export_rosters";
            this.export_rosters.Size = new System.Drawing.Size(104, 23);
            this.export_rosters.TabIndex = 3;
            this.export_rosters.Text = "Export to Excel";
            this.export_rosters.UseVisualStyleBackColor = true;
            // 
            // import_roster
            // 
            this.import_roster.Location = new System.Drawing.Point(39, 634);
            this.import_roster.Name = "import_roster";
            this.import_roster.Size = new System.Drawing.Size(104, 23);
            this.import_roster.TabIndex = 4;
            this.import_roster.Text = "Import from Excel";
            this.import_roster.UseVisualStyleBackColor = true;
            // 
            // manage_rovers
            // 
            this.manage_rovers.Location = new System.Drawing.Point(899, 634);
            this.manage_rovers.Name = "manage_rovers";
            this.manage_rovers.Size = new System.Drawing.Size(135, 23);
            this.manage_rovers.TabIndex = 5;
            this.manage_rovers.Text = "Manage Rovers";
            this.manage_rovers.UseVisualStyleBackColor = true;
            this.manage_rovers.Click += new System.EventHandler(this.manage_rovers_Click);
            // 
            // Build_roster
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1073, 669);
            this.Controls.Add(this.manage_rovers);
            this.Controls.Add(this.import_roster);
            this.Controls.Add(this.export_rosters);
            this.Controls.Add(this.list_of_rovers);
            this.Controls.Add(this.tabControl1);
            this.Name = "Build_roster";
            this.Text = "Roster your Rovers";
            this.Load += new System.EventHandler(this.Build_roster_Load);
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage aic_roster_builder;
        private System.Windows.Forms.TabPage chic_roster_builder;
        private System.Windows.Forms.TabPage huxley_roster_builder;
        private System.Windows.Forms.ListBox list_of_rovers;
        private System.Windows.Forms.Button export_rosters;
        private System.Windows.Forms.Button import_roster;
        private System.Windows.Forms.Button manage_rovers;
    }
}