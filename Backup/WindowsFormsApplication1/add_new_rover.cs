﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class add_new_rover : Form
    {
        public add_new_rover()
        {
            InitializeComponent();
        }

        private void add_new_rover_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void rover_alternate_phone_number_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            clear_form();
        }

        private void submit_new_rover_form_Click(object sender, EventArgs e)
        {

        }

        private void rover_name_Click(object sender, EventArgs e)
        {
            rover_name.Text = "";
        }

        private void rover_phone_number_Click(object sender, EventArgs e)
        {
            rover_phone_number.Text = "";
        }

        private void rover_alternate_phone_number_Click(object sender, EventArgs e)
        {
            rover_alternate_phone_number.Text = "";
        }

        private void clear_form()
        {
            rover_name.Text = "";
            rover_phone_number.Text = "";
            rover_alternate_phone_number.Text = "";
        }
    }
}
